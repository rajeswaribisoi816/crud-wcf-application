﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CRUDwcfApplication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string Insert(InsertUser value);

        [OperationContract]
        gettestdata GetInfo();

        [OperationContract]
        DataSet SearchUser(GetUser value);

        [OperationContract]
        string Update(UpdateUser value);

        [OperationContract]
        string Delete(DeleteUser value);

    }

    [DataContract]
    public class gettestdata
    {
        [DataMember]
        public DataTable usertab
        {
            get;
            set;
        }
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class InsertUser
    {
        int uid;
        string name = string.Empty;
        string cell = string.Empty;

        [DataMember]
        public int UID
        {
            get { return uid; }
            set { uid = value; }
        }
        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public string Cell
        {
            get { return cell; }
            set { cell = value; }
        }
    }

    [DataContract]
    public class UpdateUser
    {
        int uid;
        string name;
        string cell;

        [DataMember]
        public int UID
        {
            get { return uid; }
            set { uid = value; }
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public string Cell
        {
            get { return cell; }
            set { cell = value; }
        }
    }

    [DataContract]
    public class DeleteUser
    {
        int uid;

        [DataMember]
        public int UID
        {
            get { return uid; }
            set { uid = value; }
        }
    }

    [DataContract]
    public class GetUser
    {
        int uid;

        [DataMember]
        public int UID
        {
            get { return uid; }
            set { uid = value; }
        }
    }
}
